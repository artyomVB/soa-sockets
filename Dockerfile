FROM python:3.9.10

EXPOSE 8080

COPY . ./app

WORKDIR app

RUN pip install -r requirements.txt

CMD ["python", "server.py"]
