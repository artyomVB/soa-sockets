import socket
import pickle
import sounddevice as sd
import numpy as np
from scipy.io.wavfile import write

curr = None


class Client:
    def __init__(self, name):
        self.sock_for_rec = socket.socket()
        self.sock_for_sending = socket.socket()
        self.flag = False
        self.name = name
        self.members = set()
        self.now_talking = None

    def process_data(self, data):
        struct = pickle.loads(data)
        if struct["type"] == "connect":
            self.members.add(struct["name"])
        elif struct["type"] == "disconnect":
            self.members.remove(struct["name"])
        else:
            return struct["name"], struct["data"]
        return None

    def callback(self, indata, outdata, frames, time, status):
        try:
            data = self.sock_for_rec.recv(32768)
            self.sock_for_rec.send("Approve".encode())
            res = self.process_data(data)
            if res is None:
                outdata[:] = np.zeros((frames, 2))
                self.now_talking = None
            else:
                self.now_talking = res[0]
                outdata[:] = res[1]
        except BlockingIOError:
            self.now_talking = None
            outdata[:] = np.zeros((frames, 2))
        global curr
        if curr is None:
            curr = outdata
        else:
            curr = np.concatenate((curr, outdata))
        if self.flag:
            struct = {
                "type": "data",
                "name": self.name,
                "data": indata,
            }
            self.sock_for_sending.send(pickle.dumps(struct))

    def connect(self, addr, port):
        self.sock_for_sending.connect((addr, port))
        struct = {
            "type": "sender",
            "name": self.name,
        }
        self.sock_for_sending.send(pickle.dumps(struct))
        self.sock_for_sending.recv(1024)
        self.sock_for_rec.connect((addr, port))
        struct = {
            "type": "reciever",
            "name": self.name,
        }
        self.sock_for_rec.send(pickle.dumps(struct))
        data = self.sock_for_rec.recv(1024)
        self.members = pickle.loads(data)
        self.sock_for_rec.setblocking(False)
        with sd.Stream(blocksize=1024, callback=self.callback):
            print("Push x to start/stop to talking\nPush c to see who are online\nPush ? to see who is talking\nPush any button to exit\n")
            while True:
                symb = input()
                if symb == "x":
                    self.flag = not self.flag
                elif symb == "c":
                    print(self.members)
                elif symb == "?":
                    if self.now_talking is None:
                        print("Nobody")
                    else:
                        print(self.now_talking)
                else:
                    break
        self.sock_for_rec.close()
        self.sock_for_sending.close()


if __name__ == "__main__":
    print("Introduce yourself:")
    n = input()
    client = Client(n)
    client.connect('127.0.0.1', 8080)
    write('output' + n + '.wav', 44100, curr)
