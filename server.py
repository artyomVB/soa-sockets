import pickle
import queue
import socket
import threading

ids_to_names = {}
queues = {}


def reciever(conn, name):
    conn.setblocking(False)
    while True:
        chunk = queues[name].get()
        conn.send(chunk)
        while True:
            try:
                conn.recv(1024)
                break
            except BlockingIOError:
                pass


def sender(conn, name):
    print(name)
    while True:
        data = conn.recv(32768)
        if not data:
            break
        for key in queues.keys():
            if key != name:
                queues[key].put(data)
    del queues[name]
    struct = {
        "type": "disconnect",
        "name": name,
    }
    for key in queues.keys():
        queues[key].put(pickle.dumps(struct))
    conn.close()


def thread_func(conn):
    data = conn.recv(1024)
    flag = True
    query = None
    while flag:
        try:
            query = pickle.loads(data)
            flag = False
        except pickle.UnpicklingError:
            data += conn.recv(1024)
            flag = True

    if query["type"] == "reciever":
        conn.send(pickle.dumps(set(queues.keys())))
        reciever(conn, query["name"])
    else:
        struct = {
            "type": "connect",
            "name": query["name"]
        }
        for key in queues.keys():
            data = pickle.dumps(struct)
            queues[key].put(data)
        queues[query["name"]] = queue.Queue()
        conn.send("Ok".encode())
        sender(conn, query["name"])


class Server:
    def __init__(self, port):
        self.sock = socket.socket()
        self.sock.bind(('', port))
        self.sock.listen()

    def start_sever(self):
        while True:
            conn, addr = self.sock.accept()
            t = threading.Thread(target=thread_func, args=(conn, ))
            t.start()


if __name__ == "__main__":
    server = Server(8080)
    server.start_sever()
